from download_novel import *


def find_essay_links(soup):
    essay_links_all = soup.find_all('table', attrs={'class': 'list'})
    if len(essay_links_all) == 0:
        return None, None
    links, names = [], []
    for essay_links in essay_links_all:
        essay_links = essay_links.find_all('a')
        link_names = list(map(lambda l: l.get_text(), essay_links))
        essay_links = list(map(lambda l: l['href'], essay_links))
        essay_links = list(map(lambda l: base_url + l, essay_links))
        links += essay_links
        names += link_names
    return links, names


def download_essay_chapter(base_link, fname):
    print(base_link)
    #r = requests.get(base_link)
    #s = bs4.BeautifulSoup(r.text, 'html.parser')
    #name = s.find('div', attrs={'class': 'galpohead'}).get_text()
    #print('Downloading chapter {}'.format(name))
    if os.path.exists(fname):
        print('Skipping')
        return
    with open(fname, 'w') as f:
        fetch_pages_and_write(base_link, {'class': 'content clear-block'}, f)


def find_and_download_essays(base_link, pname, output_dir):
    print(base_link)
    r = requests.get(base_link)
    s = bs4.BeautifulSoup(r.text, 'html5lib')
    essay_links, link_names = find_essay_links(s)
    if essay_links is None:
        download_essay_chapter(base_link, os.path.join(output_dir, pname))
    else:
        for link, name in zip(essay_links, link_names):
            name = name.replace('/', '-')
            print('Downloading {}'.format(pname + '_' + name))
            find_and_download_essays(link, pname + '_' + name, output_dir)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: python3 {} base_link output_dir'.format(sys.argv[0]))
        exit()

    base_link = sys.argv[1]
    output_dir = sys.argv[2]
    find_and_download_essays(base_link, '', output_dir)
