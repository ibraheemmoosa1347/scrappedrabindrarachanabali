import bs4
import requests
import os
import sys
import time


base_url = 'https://rabindra-rachanabali.nltr.org'


def next_page_link(soup):
    next_link = soup.find('div',
                          attrs={'id': 'rightnavigation'})
    if next_link is None:
        return None
    next_link = next_link.find('a')['href']
    next_link = base_url + next_link
    return next_link


def fetch_pages_and_write(init_link, text_attrs, f):
    next_link = init_link
    page_count = 0
    while True:
        page_count += 1
        print('Fetching page {}'.format(page_count))
        r = requests.get(next_link)
        s = bs4.BeautifulSoup(r.text, 'html.parser')
        text = s.find_all('div', attrs=text_attrs)
        text = ' '.join(map(lambda t: t.get_text(), text))
        text += '\n'
        f.write(text)
        next_link = next_page_link(s)
        if next_link is None:
            break
        print(next_link)
        time.sleep(1)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: python3 {} base_link output_dir'.format(sys.argv[0]))
        exit()

    base_link = sys.argv[1]
    output_dir = sys.argv[2]
    print('Getting base page')
    r = requests.get(base_link)
    s = bs4.BeautifulSoup(r.text, 'html.parser')
    name = s.find('div', attrs={'class': 'content clear-block'}).get_text()
    print('Downloading {}'.format(name))
    next_link = next_page_link(s)

    with open(os.path.join(output_dir, name), 'w') as f:
        fetch_pages_and_write(base_link, {'class': 'upanyas'}, f)
